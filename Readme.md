# HowTo

## Create virtualenv
```
virtualenv venv -p python3
source venv/ocac/bin/activate
pip3 install -r requirements.txt
```
## Adjust settings

see
- docs/conf.py
- docs/index.rst

## Add markdown files
copy it in docs/static folder

## build html

```
cd docs
make html
```
