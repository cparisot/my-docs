
#export SLACK_URL="https://hooks.slack.com/services/TB6RWT6HX/..."
export REPORT_URL="https://s3-eu-west-1.amazonaws.com/$BUCKET_NAME/html/index.html"
#handle error
set -o errexit -o nounset

cd docs
make html

# Test AWS connection
aws sts get-caller-identity
# upload result
aws s3 cp /opt/docs/nbuild/ s3://$BUCKET_NAME/ --recursive
echo "static sites will be available on $REPORT_URL"

# Notify slack
# curl -X POST -H 'Content-type: application/json' \
#       --data '{"attachments": [{"color": "#36a64f","mrkdwn_in": ["text"], "text": " successfully finish on *'$BASE_URL_ENV'*! :white_check_mark:\n You can find report result <'$REPORT_URL'|here>  "}]}' \
#       $SLACK_URL
