# JENKINS

## Procédure pour démarrer un JOB Jenkins depuis slack.


### installer les plugins nécessaires

Sur l’interface Jenkins depuis « Manage Jenkins/manage plugins » ajouter les plugins suivants :
- Pour lancer un job avec des paramètres
**Build Authorization Token Root Plugin**  (Exécuter un job avec un curl)

- **Pre SCM BuildStep Plugin** (Effectuer des actions pre build par ex récupérer le commit a puller)

- **Environment Injector Plugin** (injecter des paramètres au contexte Jenkins)


- Pour notifier un channel slack de l’état d’un job
**Slack Notification Plugin**
(configurer la notification voir le lien suivant qui est autoportant)
https://medium.com/appgambit/integrating-jenkins-with-slack-notifications-4f14d1ce9c7a  



Let’s configure stuff
Dans le job Jenkins cible  

### Configurer les plugins nécessaires dans jenkins

- Exécuter un job avec un curl :
  Dans le job cible sous « build Triggers » cocher « trigger build remotely” et choisir le token de ce job *
  ![](../img/jenkins1.png)

  Créer un utilisateur jenkins ou utiliser un existant et récupérer son API key
    ![](../img/jenkins2.png)
    Il est nécessaire est que cet utilisateur ait suffisamment de droits pour déclencher le job.
  Exécutez la commande CURL ci-dessous pour vérifier si l'utilisateur peut appeler le job.

  Remplacer le contenu dans <> par les informations appropriées

  **WARNING** si votre user contient un @ par exemple encodé le ;) exemple

  « truc@chose.com » deviendra «truc%40chose.com»

  ```
  Curl https://<user>:<apikey>@truc.jenkins.com/<projet>/job/<domaine>/job/<nomdu job>/buildWithParameters?token=<your token define * >
  ```

- Effectuer des actions pre build (par ex récupérer le commit a pull;)

  Dans le job cible sous « bindings » cocher « Run buildstep before SCM run »
    ![](../img/jenkins3.png)
  Le script suivant concatène texte (les paramètres que l’on donnera après l’url )

  ```
  #!/bin/bash
  ##permet de splitter text dans un tableau avec comme délimiteur espace
  stringarray=($text)

  ## apres on écrit les variables souhaitées dans un fichier, car le contexte de ce script (pre-build) n’est pas partagé avec le contexte du build principal.
  echo "VERSION=${stringarray[0]}" > variables.properties
  echo "COMMIT_TO_PUSH=${stringarray[1]}" >> variables.properties
  echo "ENV=${stringarray[2]}" >> variables.properties
  ```
  Injecter des paramètres au contexte Jenkins

  Dans le job cible sous « bindings » en dessous du execute shell précédent
  ![](../img/jenkins3.png)

### Sur interface slack

    Sous `https://<votre espace>.slack.com/apps/manage/custom-integrations`

  ![](../img/slack1.png)

Ajouter votre configuration
  ![](../img/slack2.png)

### Lets test it !

  Allez sur votre slack je conseille le chan où vous recevrez vos notifications Jenkins car /commade proposé ne renvoie pas de message ;)
  ![](../img/slack3.png)

  Vous retrouvé ici le nom de ma commande puis les paramètres texte ** qui correspondent dans l’ordre
   BETTY_VERSION COMMIT_TO_PUSH ENV

   Si vous avez bien configurer vos notifications vous aurez :
    ![](../img/slack4.png)



Allez plus loin: l'étape d'après pourrait être de remplacer la /commande par la conception d'un Bot pour enrichir les échanges avec l'utilisateur.
