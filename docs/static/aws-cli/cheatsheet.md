# aws-cli

## Get id of running instance
```
aws ec2 describe-instances \
                        --filters "Name=tag:Name,Values=ec2-$ENV*" "Name=instance-state-name,Values=running"\
                        --output text \
                        --query 'Reservations[0].Instances[0].InstanceId'
```

## SSM send command
```
aws ssm send-command \
      --instance-ids $(aws ec2 describe-instances \
                        --filters "Name=tag:Name,Values=ec2-$ENV*" "Name=instance-state-name,Values=running"\
                        --output text \
                        --query 'Reservations[0].Instances[0].InstanceId') \
      --document-name "AWS-RunShellScript" \
      --comment "apply conf on $ENV" \
      --parameters '{"commands":["./update_efs.sh '$arg1' '$arg2' "],"executionTimeout":["600"],"workingDirectory":["/home/ec2-user"]}'
```
##  S3 push recursif
`aws s3 cp $(ls -d ./results/ehs*) s3://s3-gatling-results-bucket/$REPORT_NAME --recursive`

## Get AWS account info from local
```
export AWS_ACCESS_KEY_ID="$(aws configure get aws_access_key_id --profile default)"
export AWS_SECRET_ACCESS_KEY="$(aws configure get aws_secret_access_key --profile default)"
export AWS_DEFAULT_REGION="$(aws configure get region --profile default)"
export AWS_SESSION_TOKEN="$(aws configure get aws_session_token)"
```
## delete ecr
```
 aws ecr describe-repositories | jq -r '.repositories[].repositoryName' | egrep '\/(vimubuntu|app)$' | xargs -L1 aws ecr delete-repository --force --repository-name
```
