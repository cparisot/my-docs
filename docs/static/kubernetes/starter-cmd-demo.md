# kubernetes-demo

## minikube

```
minikube start
minikube addons enable ingress
```


## kubernetes-commandes

### create NS
```
kubectl create ns demo
```
se placer dans votre namespace

```
kubectl config set-context minikube --namespace demo
ou kubens demo
```

### lister les ressources disponibles
```
kubectl api-resources
```

### votre 1er deploy
[info lien vers l'application de demo](https://gitlab.com/cparisot/side-project/tree/demo)
```
kubectl run --image=cyparisot/side-project:demo demo-app --port=8080
```
#### check what happen
```
kubectl get all
kubectl get deploy demo-app -o yaml
```
**Hint** dry-run is your friend
```
kubectl create deploy test --image=alpine --dry-run -o yaml
```

### votre 1er rolling update
```
kubectl set image deploy/demo-app demo-app=cyparisot/side-project:demo2
```
#### check what happen
```
kubectl get rs -o wide
kubectl get deploy demo-app -o yaml
kubectl get all
kubectl describe deploy/demo-app
```

### votre 1er scale
```
kubectl scale --replicas=2 deploy/demo-app
```
#### check what happen
```
kubectl get rs -o wide
kubectl get pods
```

### votre 1er service
```
kubectl expose deploy demo-app --port=80 --target-port=8080
```

#### check what happen
```
kubectl get svc
```

### votre 1ere ingress
```
kubectl apply -f ingress-demo.yaml
```

*WARN DNS*
```
echo "$(minikube ip) yolo.octo.io" | sudo tee -a /etc/hosts
```

#### check what happen
```
kubectl get ing
```


### votre 1 deploy ZDD

```

while true; do sleep 1; curl http://yolo.octo.io/heartbeat; echo -e '\n'$(date);done
kubectl set image deploy/demo-app demo-app=cyparisot/side-project:demo
```

#### check what happen
```
kubectl get all -o wide
```

#### Use configMap (optinal)
- create cm
```
kubectl create configmap myapp-config --from-file=app/static/index.html
```
- update demo
```
kubectl apply -f demo.yaml
```
- patch
```
kubectl patch deploy demo-app -p "{\"spec\":{\"template\":{\"metadata\":{\"annotations\":{\"date\":\"`date +'%s'`\"}}}}}"
```
## ANNEXES
ingress-demo.yaml
```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: ingress-demo
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  backend:
    serviceName: default-http-backend
    servicePort: 80
  rules:
  - host: yolo.octo.io
    http:
      paths:
      - path: /
        backend:
          serviceName: demo-app
          servicePort: 80
```

deploy demo.yaml
```yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  labels:
    run: demo-app
  name: demo-app
  namespace: demo
spec:
  progressDeadlineSeconds: 600
  replicas: 2
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      run: demo-app
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        run: demo-app
    spec:
      volumes:                              # <== Ajout pour configmap
      - name: config-volume                 # <== Ajout pour configmap
        configMap:                          # <== Ajout pour configmap
          name: myapp-config                # <== Ajout pour configmap
      containers:
      - image: cyparisot/side-project:demo3
        imagePullPolicy: IfNotPresent
        name: demo-app
        ports:
        - containerPort: 8080
          protocol: TCP
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:                       # <== Ajout pour config map
        - name: config-volume               # <== Ajout pour config map
          mountPath: /root/static/          # <== Ajout pour config map
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30

```
## Bonus
- dashboard `minikube dashboard`
- port forward
```
kubectl --namespace demo port-forward $(kubectl --namespace demo get pod -l run=demo-app -o template --template="{{(index .items 0).metadata.name}}") 8080
```
- create cluster (see kops.md)

## Bonus, if on fire
si on scale un RS ? pourquoi ca marche pas ?


## clean all
```
kubectl delete ns demo
```
