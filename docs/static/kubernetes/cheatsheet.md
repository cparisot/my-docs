# kubernetes

## minikube

### minikube reset
```
minikube delete
```
### minikube specify version
```
minikube start --kubernetes-version=v1.9.0
```
### minikube inside docker
```
minikube start —vm-driver=none
```
### update minikube
```
minikube update-check                  #check if update needed

sudo rm -rf /usr/local/bin/minikube    # unlink existing minikube
brew update                            # update brew itself
brew cask reinstall minikube           # reinstall latest minikube
```
### partage de sa registry docker local avec minikube
```
eval $(minikube docker-env)

start registery local :
docker container run -d -p 5000:5000 registry
```
## kubernetes-commandes
### get status of K8s component
```
kubectl get cs
```
### get custom information
```
kubectl get nodes \
 -o=custom-columns=NAME:.metadata.name,CPU:.status.allocatable.cpu
```
### explain commande
```
k explain deploy --recursive
```

### run busybox inside cluster
```
kubectl run -ti --image=odise/busybox-curl busybox --rm
```

### be admin on cluster GCP
```
kubectl create clusterrolebinding cluster-admin-binding-<<trigramme>> --clusterrole=cluster-admin --user=$(gcloud config get-value core/account)
```

### delete list
```
k delete -n kanary $(k get po -o name -n kanary)
k delete $(k get po -o name -n logging | grep fluent)
```

### force pod delete

```
k delete po <name> --grace-period=0 --force
```

### view secret
```
 k get secret alertmanager-main
 ```
### replace secret
```
 kubectl create secret generic alertmanager-main --from-literal=alertmanager.yaml="$(< alertmanager.yaml)" --dry-run -oyaml | kubectl  replace secret --filename=-
```

## Dashboard
install : dashboard
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
```
### Open tunnel
```
kubectl proxy
```
### Get token to login
```
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep  clusterrole-aggregation-controller-token | awk '{print $1}')
```

#### bash loop

```
$ while true; do sleep 1; curl http://127.0.0.1:30002/hit; echo -e '\n'$(date);done
while true; do sleep 1; curl http://metrics-app:8080 ;done
while true; do sleep 1; date ;done
while true; echo date '\n' >> fillingDisk.log ;done
```

### force to patch
```
k patch deploy haproxy-kanary -p "{\"spec\":{\"template\":{\"metadata\":{\"annotations\":{\"date\":\"`date +'%s'`\"}}}}}"
```

### port-forward
```
  kubectl port-forward -n monitoring prometheus-k8s-0 9090

  kubectl port-forward $(kubectl get  pods --selector=id=kibana-logging -n logging --output=jsonpath="{.items..metadata.name}") -n logging  5601

  kubectl port-forward $(kubectl get  pods --selector=app=grafana -n monitoring --output=jsonpath="{.items..metadata.name}") -n monitoring 3000
```


### list docker images on cluster
```
kubectl get po --all-namespaces -o jsonpath="{.items[*].spec.containers[*].image}" | tr -s '[[:space:]]' '\n'| sort -u
```

## customize bash

utilitaire shell [kube_ps1](https://github.com/jonmosco/kube-ps1) pour modifier notre prompt pour clairement visualiser le contexte actif :

```
dev $ kubeon
dev (☸ |kubernetes-admin@kubernetes:default) $
```
l’outil [kubectx](https://github.com/ahmetb/kubectx) permet la gestion de nouveau contexte pour travailler dans un namespace :

```
dev (☸ |kubernetes-admin@kubernetes:default) $ kubectl config set-context cluster-prod --cluster=kubernetes --user=kubernetes-admin --namespace=prod
Context "cluster-prod" created.
```
