# KOPS
Pour POCer tranquille voici comment installer K8S en 2 lignes de cmd
(*une ligne avec "--yes", si vous etes une machine et que vous avez confiance en vos configs*)

  prérequis
  -kops installé : https://github.com/kubernetes/kops
  -etre connecté à la cli AWS (avec votre MFA )
  -avoir un bucket S3 pour le stockage  (ici mon cas d'exemple cparisot-test-K8s)


## Install Cluster via kops
  ```
  export AWS_PROFILE=octops
  export KOPS_STATE_STORE=s3://cparisot-test-k8s

  kops create cluster --cloud=aws \
  --dns-zone=bogops.io \
  --master-size=t2.small \
  --master-zones=eu-west-1a \
  --network-cidr=10.0.0.0/22 \
  --node-count=3 \
  --node-size=t2.small \
  --zones=eu-west-1a,eu-west-1b,eu-west-1c \
  --name=k8stest.bogops.io \
  --cloud-labels "Trigramme=cypa" \
  --kubernetes-version=1.9.6
  ```
```
  kops update cluster k8stest.bogops.io --yes
```

pour ssh  sur les machines créé par kops
```
kops get secrets --type sshpublickey admin
ssh admin@<<iphost>>
```
source https://github.com/kubernetes/kops/blob/master/docs/security.md
  -- fin de l'installation --

explain de la cmd: `kops create cluster`
- --cloud=aws \ == specifier provider
- --dns-zone=bogops.io \  == choix du dns
- --master-size=t2.small \ == choix du type de VM pour les masters
- --master-zones=eu-west-1a \ == AZ du(des) master
- --network-cidr=10.0.0.0/22 \  == cidr
- --node-count=3 \ == nombre de node
- --node-size=t2.small \ == choix du type de VM pour les
- --zones=eu-west-1a,eu-west-1b,eu-west-1c \ == AZ des nodes
- --name=k8stest.bogops.io \  == nom du cluster
- --cloud-labels "Trigramme=cypa" == les tags que vous souhaiter ajouter

kops gère la création du vpc des subnets (en gros toute la config AWS ) et la config de votre kubectl


pensez à tous supprimer apres votre test :
```
kops delete cluster k8stest.bogops.io --yes
```

## install applicative
```
kubectl --namespace=kube-system create -f https://raw.githubusercontent.com/kubernetes/kops/master/addons/kubernetes-dashboard/v1.8.1.yaml
https://api.k8stest.bogops.io/ui


kubectl -n test run hello-world --image=tutum/hello-world --replicas=3

kubectl -n test expose deployment hello-world --port=8080 --target-port=80 --type=NodePort

kubectl -n test port-forward hello-world-47050946-gtw5f 8080:80
```
```
kops edit cluster k8stest...
```
change `authorization` de `alwaysAllow: {}` à `rbac: {}`

```
kops validate cluster k8stest.bogops.io

kops update cluster k8stest.bogops.io --yes
```
