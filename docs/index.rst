Welcome to an OPS documentation!
=================================
If you to participate send a merge request on https://gitlab.com/cparisot/my-docs/tree/master/docs/static

.. toctree::
   :glob:

   /static/**


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
