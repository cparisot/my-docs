terraform {
  required_version = ">=0.11.8"
}

provider "aws" {
  profile = "${var.profile}"
  region  = "${var.aws_region}"
}
