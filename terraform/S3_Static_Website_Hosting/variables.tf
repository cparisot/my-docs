# AWS ACCOUNT INFORMATION

variable "aws_region" {
  description = "Region that the instances will be created"
  default     = "eu-west-1"
}

variable "profile" {
  description = "Name of your AWS-Cli profile"
  default ="default"
}


variable "hosted_public_zone_id" {
  description = "The application's public hosted zone"
}

# variable "website_domain" {
#   description = "The application's public hosted zone"
# }
